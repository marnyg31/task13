﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13_MovieAPI.DTO.Characters
{
    //DTO object for Actor model
    public class CharacterDTO
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Alias{get;set;}
        public bool IsMale { get; set; }
        public Uri Picture { get; set; }
        public List<string> MoviesWithCharacter { get; set; }
    }
}
