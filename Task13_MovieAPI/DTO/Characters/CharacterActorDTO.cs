﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13_MovieAPI.DTO.Characters
{
    //DTO object for Actor and Character combination 
    public class CharacterActorDTO
    {
        public int  CharacterId { get; set; }
        public string CharacterName { get; set; }
        public int ActorId { get; set; }
        public string ActorName { get; set; }
    }
}
