﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task13_MovieAPI.DTO.Characters;

namespace Task13_MovieAPI.DTO.Movies
{
    //DTO object for movie model with all characters and actors in move
    public class MovieCharacterActorDTO
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public List< CharacterActorDTO> CharacterActorDtos { get; set; }

    }
}
