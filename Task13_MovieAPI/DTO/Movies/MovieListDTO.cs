﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task13_MovieAPI.DTO.Movies
{
    //DTO object for List of Movies model
    public class MovieListDTO
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
    }
}
