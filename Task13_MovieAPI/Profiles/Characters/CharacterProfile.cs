﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Task13_MovieAPI.DTO.Characters;
using Task13_MovieAPI.Models;

namespace Task13_MovieAPI.Profiles.Characters
{
    //Profile for automapper to map between Character and its DTO
    public class CharacterProfile:Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterDTO>()
                .ForMember(
                    CharDTO=>CharDTO.MoviesWithCharacter,
                opt=>opt.MapFrom(
                        cr=>cr.MovieCharacter.Select(mc=>mc.Movie.MovieTitle)
                        ));
        }

    }
}
