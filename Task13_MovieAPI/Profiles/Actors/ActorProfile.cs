﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Task13_MovieAPI.DTO.Actors;
using Task13_MovieAPI.Models;

namespace Task13_MovieAPI.Profiles

{
    //Profile for automapper to map between Actor and its DTO
    public class ActorProfile:Profile
    {
        public ActorProfile()
        {
            CreateMap<Actor, ActorDTO>();
        }
    }
}
