﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Task13_MovieAPI.DTO.Movies;
using Task13_MovieAPI.Models;

namespace Task13_MovieAPI.Profiles.Movies
{
    //Profile for automapper to map between Movie and its DTO
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieDTO>()
                .ForMember(
                movieDTO => movieDTO.MovieCharacters,
                opt => opt.MapFrom(
                    m => m.MovieCharacter.Select(mc=>mc.Character.FullName) ) )
                .ForMember(
                movieDTO => movieDTO.Franchise,
                opt => opt.MapFrom(
                    m => m.Franchise.Name ));

            //CreateMap<Movie, MovieListDTO>()
        }
    }
}
