﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Task13_MovieAPI.DTO.Actors;
using Task13_MovieAPI.DTO.Movies;
using Task13_MovieAPI.Models;

namespace Task13_MovieAPI.Profiles.Movies
{
    //Profile for automapper to map between MovieCharacter and its DTO
    public class MovieCharacterProfile:Profile
    {
        public MovieCharacterProfile()
        {
            CreateMap<MovieCharacter, MovieCharacterDTO>();
        }
    }
}
