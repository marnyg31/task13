﻿using System;
using System.Collections.Generic;

namespace Task13_MovieAPI.Models
{
    //Model definition for Character
    public class Character
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Alias{get;set;}
        public bool IsMale { get; set; }
        public Uri Picture { get; set; }
        public List<MovieCharacter> MovieCharacter { get; set; }
    }
}
