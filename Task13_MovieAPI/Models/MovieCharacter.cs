﻿using System;
using System.Collections.Generic;

namespace Task13_MovieAPI.Models
{
    //Model definition for MovieCharacter (binding table for many to many between Movie an Character)
    public class MovieCharacter
    {
        public int MovieId { get; set; }
        public Movie Movie{get;set;}
        public int CharacterId { get; set; }
        public Character Character { get; set; }
        public Uri Picture { get; set; }
        public Actor Actor { get; set; }
        public int ActorId { get; set; }
    }
}
