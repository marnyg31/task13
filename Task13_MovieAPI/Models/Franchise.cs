﻿using System.Collections.Generic;

namespace Task13_MovieAPI.Models
{
    //Model definition for Franchise
    public class Franchise
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Movie> Movies { get; set; }
    }
}
