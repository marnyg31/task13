﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task13_MovieAPI.DTO.Characters;
using Task13_MovieAPI.DTO.Movies;
using Task13_MovieAPI.Models;

namespace Task13_MovieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        //This is the controller class for the Movie model it defines the endpoints for reading and manipulating the table in the database
        private readonly ProjectDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(ProjectDbContext context,IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMovies()
        {
            var movies =await _context.Movies.Include(m=>m.Franchise).Include(m=>m.MovieCharacter).ThenInclude(mc=>mc.Character).ToListAsync();
            return movies.Select(movies => _mapper.Map<MovieDTO>(movies)).ToList();
        }

        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDTO>> GetMovie(int id)
        {
            var movie =  _context.Movies.Include(m=>m.MovieCharacter).ThenInclude(mc=>mc.Character).FirstOrDefault(m=>m.Id==id);

            if (movie == null)
            {
                return NotFound();
            }

            var movieDTO = _mapper.Map<MovieDTO>(movie);
            return movieDTO;
        }

        // GET: api/Movies/5/CharactersAndActors
        [HttpGet("{id}/CharactersAndActors")]
        public async Task<ActionResult<MovieCharacterActorDTO>> GetCharactersAndActors(int id)
        {
            var movie = _context.Movies.Find(id);
            var movieChacacter = _context.MovieCharacters.
                Include(mc => mc.Actor).
                Include(mc => mc.Character).
                Where(mc=>mc.MovieId==id);

            if (movie == null||movieChacacter==null)
            {
                return NotFound();
            }

            MovieCharacterActorDTO mca=new MovieCharacterActorDTO()
            {
                Id=movie.Id,
                MovieTitle = movie.MovieTitle,
                CharacterActorDtos = movieChacacter.Select(mc=> new CharacterActorDTO()
                {
                    ActorId = mc.ActorId,
                    ActorName = mc.Actor.FirstName+" "+mc.Actor.LastName,
                    CharacterId = mc.CharacterId,
                    CharacterName = mc.Character.FullName
                }).ToList()
            };

            return mca;
        }

        // PUT: api/Movies/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, Movie movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Movies
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieDTO>> PostMovie(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = movie.Id },_mapper.Map<MovieDTO>( movie));
        }

        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieDTO>> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return _mapper.Map<MovieDTO>(movie);
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
