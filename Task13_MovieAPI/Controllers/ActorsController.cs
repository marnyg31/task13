﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task13_MovieAPI.DTO.Actors;
using Task13_MovieAPI.DTO.Characters;
using Task13_MovieAPI.DTO.Movies;
using Task13_MovieAPI.Models;

namespace Task13_MovieAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        //This is the controller class for the Actor model it defines the endpoints for reading and manipulating the table in the database
        private readonly ProjectDbContext _context;
        private readonly IMapper _mapper;
        public ActorsController(ProjectDbContext context,IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        // GET: api/Actors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActorDTO>>> GetActors()
        {
            return await _context.Actors.Select(s => _mapper.Map<ActorDTO>(s)).ToListAsync();
        }

        // GET: api/Actors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActorDTO>> GetActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);
            var actorDTO =_mapper.Map<ActorDTO>(actor);


            if (actor == null)
            {
                return NotFound();
            }

            return actorDTO;
        }
        // GET: api/Actors/5/Characters
        [HttpGet("{id}/Characters")]

        // GET: api/Actors/5/Movies
        public async Task<ActionResult<List<CharacterDTO>>> GetCharactersOfActor(int id)
        {
            var actor = await _context.Actors.
                Include(a=>a.MovieCharacter).
                ThenInclude(mc=>mc.Character).
                Include(c=>c.MovieCharacter).
                ThenInclude(mc=>mc.Movie).
                Where(a=>a.Id==id).
                FirstOrDefaultAsync();

            if (actor == null)
            {
                return NotFound();
            }
            
            var characters = actor.MovieCharacter.Select(mc => mc.Character).Distinct();

            return characters.Select(c=>_mapper.Map<CharacterDTO>(c)).ToList();
        }
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<List<MovieDTO>>> GetMoviesOfActor(int id)
        {
            var actor = await _context.Actors.
                Include(a=>a.MovieCharacter).
                ThenInclude(mc=>mc.Character).
                Include(c=>c.MovieCharacter).
                ThenInclude(mc=>mc.Movie).
                Where(a=>a.Id==id).
                FirstOrDefaultAsync();


            if (actor == null)
            {
                return NotFound();
            }
            
            var movies = actor.MovieCharacter.Select(mc => mc.Movie).Distinct();

            return movies.Select(m=>_mapper.Map<MovieDTO>(m)).ToList();
        }

        // PUT: api/Actors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, Actor actor)
        {
            if (id != actor.Id)
            {
                return BadRequest();
            }

            _context.Entry(actor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Actors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ActorDTO>> PostActor(Actor actor)
        {
            _context.Actors.Add(actor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActor", new { id = actor.Id }, _mapper.Map<ActorDTO>(actor));
        }

        // DELETE: api/Actors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ActorDTO>> DeleteActor(int id)
        {
            var actor = await _context.Actors.FindAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            _context.Actors.Remove(actor);
            await _context.SaveChangesAsync();

            return _mapper.Map<ActorDTO>(actor);
        }

        private bool ActorExists(int id)
        {
            return _context.Actors.Any(e => e.Id == id);
        }
    }
}
