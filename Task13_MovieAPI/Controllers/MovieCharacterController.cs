﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Task13_MovieAPI.DTO.Movies;
using Task13_MovieAPI.Models;
using Task13_MovieAPI.Models;


namespace movie_character_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieCharactersController : ControllerBase
    {
        //This is the controller class for the MovieCharacter model it defines the endpoints for reading and manipulating the table in the database
        private readonly ProjectDbContext _context;
        private readonly IMapper _mapper;

        public MovieCharactersController(ProjectDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/movieCharacter
        [HttpGet]
        public async Task<ActionResult<List<MovieCharacterDTO>>> GetMovieCharacter()
        {

            var mc = _context.MovieCharacters;
            return mc.Select(m => _mapper.Map<MovieCharacterDTO>(m)).ToList();
        }

        // PUT: api/movieCharacter
        [HttpPut]
        public async Task<IActionResult> PutMovieCharacter(MovieCharacter movieCharactersInput)
        {

            try
            {
                _context.Entry(movieCharactersInput).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                return BadRequest();
            }

            return NoContent();
        }

        // POST: api/movieCharacter
        [HttpPost]
        public async Task<ActionResult<MovieCharacterDTO>> PostMovieCharacter(MovieCharacter movieCharactersInput)
        {

            try
            {
                _context.MovieCharacters.Add(movieCharactersInput);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                return BadRequest();
            }

            return _mapper.Map<MovieCharacterDTO>(movieCharactersInput);
        }

        // DELETE: api/movieCharacter
        [HttpDelete]
        public async Task<ActionResult<MovieCharacterDTO>> DeleteMovieCharacter(MovieCharacter movieCharactersInput)
        {

            try
            {
                _context.MovieCharacters.Remove(movieCharactersInput);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                return BadRequest();
            }

            return _mapper.Map<MovieCharacterDTO>(movieCharactersInput);
        }

        private bool MovieCharacterExists(int characterId, int movieId)
        {
            return _context.MovieCharacters.Any(e => e.CharacterId == characterId && e.MovieId == movieId);
        }

    }
}
