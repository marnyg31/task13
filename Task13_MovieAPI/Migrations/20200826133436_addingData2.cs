﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Task13_MovieAPI.Migrations
{
    public partial class addingData2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Name", "Description " },
                values: new object[,]{
                    {1,"franchise1","such a big franchise you don't need a descrition"},
                    {2,"franchise2","such a big franchise you don't need a descrition"},
                    {3,"franchise3","such a big franchise you don't need a descrition"},
                    {4,"franchise4","such a big franchise you don't need a descrition"},
                    {5,"franchise5","such a big franchise you don't need a descrition"},
                    {6,"franchise6","such a big franchise you don't need a descrition"},
                }
            );
            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "MovieTitle", "Genre", "ReleseYear", "Director", "Trailer", "FranchiseId" },
                values: new object[,]{
                    {1,"movie1","Horror",new DateTime(2000,1,1),"timmy1","https://www.youtube.com/watch?v=dQw4w9WgXcQ",1},
                    {2,"movie2","Horror",new DateTime(2000,1,1),"timmy2","https://www.youtube.com/watch?v=dQw4w9WgXcQ",2},
                    {3,"movie3","Horror",new DateTime(2001,1,1),"timmy3","https://www.youtube.com/watch?v=dQw4w9WgXcQ",3},
                    {4,"movie4","Horror",new DateTime(2001,1,1),"timmy4","https://www.youtube.com/watch?v=dQw4w9WgXcQ",4},
                    {5,"movie5","Horror",new DateTime(2001,1,1),"timmy4","https://www.youtube.com/watch?v=dQw4w9WgXcQ",4},
                    {6,"movie6","Horror",new DateTime(2001,1,1),"timmy4","https://www.youtube.com/watch?v=dQw4w9WgXcQ",4},
                    {7,"movie7","Horror",new DateTime(2002,1,1),"timmy7","https://www.youtube.com/watch?v=dQw4w9WgXcQ",5},
                    {8,"movie8","Horror",new DateTime(2003,1,1),"timmy7","https://www.youtube.com/watch?v=dQw4w9WgXcQ",5},
                }
            );
            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "FullName", "Alias", "IsMale", "Picture" },
                values: new object[,]{
                    {1,"Character Name 1","Alias1",true,"https://pbs.twimg.com/profile_images/949787136030539782/LnRrYf6e.jpg"},
                    {2,"Character Name 2","Alias2",true,"https://pbs.twimg.com/profile_images/949787136030539782/LnRrYf6e.jpg"},
                    {3,"Character Name 3","Alias3",false,"https://pbs.twimg.com/profile_images/949787136030539782/LnRrYf6e.jpg"},
                    {4,"Character Name 4","Alias4",false,"https://pbs.twimg.com/profile_images/949787136030539782/LnRrYf6e.jpg"},
                    {5,"Character Name 5","Alias5",false,"https://pbs.twimg.com/profile_images/949787136030539782/LnRrYf6e.jpg"},
                    {6,"Character Name 6","Alias6",true,"https://pbs.twimg.com/profile_images/949787136030539782/LnRrYf6e.jpg"},
                }
            );
            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "FirstName ", "MiddleName ", "LastName ", "IsMale ", "DateOfBirth ", "PlaceOfBirth ", "Biography " },
                values: new object[,]{
                    {1,"FirstName1","MiddleName1","LastName1",true,new DateTime(1990,1,1),"placeOfBirth1","let be honest, you know who FirstName1 is"},
                    {2,"FirstName2","MiddleName2","LastName2",true,new DateTime(1990,1,1),"placeOfBirth2","let be honest, you know who FirstName2 is"},
                    {3,"FirstName3","MiddleName3","LastName3",false,new DateTime(1991,1,1),"placeOfBirth3","let be honest, you know who FirstName3 is"},
                    {4,"FirstName4","MiddleName4","LastName4",false,new DateTime(1992,1,1),"placeOfBirth4","let be honest, you know who FirstName4 is"},
                    {5,"FirstName5","MiddleName5","LastName5",false,new DateTime(1992,1,1),"placeOfBirth5","let be honest, you know who FirstName5 is"},
                    {6,"FirstName6","MiddleName6","LastName6",false,new DateTime(1989,1,1),"placeOfBirth6","let be honest, you know who FirstName6 is"},
                    {7,"FirstName7","MiddleName7","LastName7",true,new DateTime(1988,1,1),"placeOfBirth7","let be honest, you know who FirstName7 is"},
                    {8,"FirstName8","MiddleName8","LastName8",true,new DateTime(1987,1,1),"placeOfBirth8","let be honest, you know who FirstName8 is"},
                }
            );
            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "MovieId", "CharacterId", "Picture", "ActorId"},
                values: new object[,]{
                    {1,1,"https://c7.alamy.com/comp/bpagk6/movie-poster-the-invisible-man-1933-bpagk6.jpg",1},
                    {1,2,"https://c7.alamy.com/comp/bpagk6/movie-poster-the-invisible-man-1933-bpagk6.jpg",1},
                    {1,3,"https://c7.alamy.com/comp/bpagk6/movie-poster-the-invisible-man-1933-bpagk6.jpg",2},
                    {1,4,"https://c7.alamy.com/comp/bpagk6/movie-poster-the-invisible-man-1933-bpagk6.jpg",3},
                    {2,1,"https://c7.alamy.com/comp/bpagk6/movie-poster-the-invisible-man-1933-bpagk6.jpg",4},
                    {2,6,"https://c7.alamy.com/comp/bpagk6/movie-poster-the-invisible-man-1933-bpagk6.jpg",5},
                    {4,1,"https://c7.alamy.com/comp/bpagk6/movie-poster-the-invisible-man-1933-bpagk6.jpg",6},
                    {5,1,"https://c7.alamy.com/comp/bpagk6/movie-poster-the-invisible-man-1933-bpagk6.jpg",7},
                    {6,1,"https://c7.alamy.com/comp/bpagk6/movie-poster-the-invisible-man-1933-bpagk6.jpg",8},
                    {4,2,"https://c7.alamy.com/comp/bpagk6/movie-poster-the-invisible-man-1933-bpagk6.jpg",8},
                    {5,2,"https://c7.alamy.com/comp/bpagk6/movie-poster-the-invisible-man-1933-bpagk6.jpg",8},
                    {6,2,"https://c7.alamy.com/comp/bpagk6/movie-poster-the-invisible-man-1933-bpagk6.jpg",8},
                }
            );

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValues: new object[] { 1, 2, 3, 4, 5, 6, 7, 8 });
            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValues: new object[] { 1, 2, 3, 4, 5, 6 });
            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValues: new object[] { 1, 2, 3, 4, 5, 6 });
            migrationBuilder.DeleteData(
                table: "MovieCharacters",
                keyColumns: new string[] { "MovieId", "CharacterId" },
                keyValues: new object[,] {
                    {1,1}, {1,2}, {1,3}, {1,4}, {2,1}, {2,6},
                    {4,1}, {5,1}, {6,1}, {4,2}, {5,2}, {6,2},
                    });
            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValues: new object[] {1,2,3,4,5,6,7,8});
        }
    }
}
