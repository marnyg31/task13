Solution for task 13

The repo contains a API which will run on port 44328 on you localhost. All the endpoints are documented with Swagger, and can be viewed and interacted with at `https://localhost:44328/swagger/index.html`. The API endpoints are tested with Postman, and the Postman collection are included in the repo at `/task13MovieAPI.postman_collection.json`, and can be imported into your local postman instance. 

Or you can click this button to get a copy of the postman collection without needing to manualy import the file.
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/0ec9bb8b6f7df03fdd2a)

Before running the API, you will need to instantiate the database by running the migrations included in the repo. this will also populate the database with some data, that can be explored for testing purposes.

